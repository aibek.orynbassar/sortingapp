package com.aibek;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class AppTest {
    private String result;

    public AppTest(String result) {
        this.result = result;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {"5 2 7 8"},
                {"6 4 5 7"},
                {"5 2 7 8 6 7"},
                {"1"},
                {"4 0 8 4 2 2 9 3 4 9"},
        });
    }


    @Test
    public void inOneToTenCase() {
        // GIVEN
        ArrayList<Integer> vals = new ArrayList<>();
        String[] temp = result.split(" ");

        for (int i = 0; i < temp.length; i++) {
            vals.add(Integer.parseInt(temp[i]));
        }
        Collections.sort(vals);
        String expected = vals.toString();

        // WHEN-THEN
        assertEquals(expected, App.sort(result));
    }
}


