package com.aibek;

import org.junit.Test;

import static org.junit.Assert.*;

public class SortingTest {

    @Test
    public void nullCase() {
        // GIVEN
        String input = null;

        // WHEN
        String actual = App.sort(input);

        // THEN
        assertEquals("Input argument is null", actual);
    }

    @Test
    public void emptyCase(){
        // GIVEN
        String input = "";

        // WHEN
        String actual = App.sort(input);

        // THEN
        assertEquals("At least 1 number", actual);
    }

    @Test
    public void moreThanTenCase(){
        // GIVEN
        String input = "4 0 8 4 2 2 9 3 4 9 11";

        //WHEN
        String actual = App.sort(input);

        //THEN
        assertEquals("Max numbers are 10", actual);
    }
}
