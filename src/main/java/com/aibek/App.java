package com.aibek;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class App {
    private static final Logger logger = LogManager.getLogger(App.class);

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        while (!scanner.hasNext("exit")) {
            System.out.println(sort(scanner.nextLine()));
        }
    }

    public static String sort(String numbers) {
        if (numbers == null) {
            logger.log(Level.ERROR, "Input argument is null");
            return "Input argument is null";
        }
        if (numbers.isEmpty()){
            return "At least 1 number";
        }
        ArrayList<Integer> result = new ArrayList<>();
        String[] temp = numbers.split(" ");
        if (temp.length > 10) {
            logger.log(Level.ERROR, "Max numbers are 10");
            return "Max numbers are 10";
        }

        for (int i = 0; i < temp.length; i++) {
            result.add(Integer.parseInt(temp[i]));
        }
        Collections.sort(result);
        return result.toString();
    }
}
